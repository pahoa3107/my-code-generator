#!/usr/bin/env node
"use strict";
const ACTIONS = ["add", "update", "delete"];

const inquirer = require("inquirer");
const colors = require("colors");
const myArgs = process.argv.slice(2);
const index = require("hygen");
const path = require("path");
const logger_1 = require("./logger");
const defaultTemplates = path.join(__dirname, "./_templates");
const fs = require("fs-extra");

const runner = (argv, callback) => {
  index
    .runner(argv, {
      templates: defaultTemplates,
      cwd: process.cwd(),
      logger: new logger_1.default(console.log.bind(console)),
      debug: !!process.env.DEBUG,
      exec: (action, body) => {
        const opts = body && body.length > 0 ? { input: body } : {};
        return require("execa").command(
          action,
          Object.assign(Object.assign({}, opts), { shell: true })
        );
      },
      createPrompter: () => require("enquirer"),
    })
    .then(async ({ success }) => {
      callback
        ? success
          ? callback()
          : process.exit(1)
        : process.exit(success ? 0 : 1);
    });
};

const chooseActions = (name) => {
  return inquirer
    .prompt([
      {
        type: "checkbox",
        name: "actions",
        message: "Choose actions",
        choices: ["Add", "Update", "Delete"],
      },
    ])
    .then(async ({ actions }) => {
      // global.actions = actions;
      const _actions = actions.filter((action) => action !== "Delete");
      return runner(["list", "new", name, `--actions=${actions}`], () =>
        runner(["detail", "new", name, `--actions=${actions}`], () => {
          if (_actions.length === 2) {
            return runner(["creator", "new", name, "--actionType=add"], () =>
              runner(["creator", "new", name, "--actionType=update"])
            );
          }
          if (_actions.length === 1) {
            return runner([
              "creator",
              "new",
              name,
              `--actionType=${_actions[0].toLowerCase()}`,
            ]);
          }
          return;
        })
      );
    });
};

const inputName = (module) => {
  inquirer
    .prompt([
      {
        type: "input",
        name: "name",
        message: "What is the module's name?",
        default: "name",
        validate: (input) => {
          return true;
        },
      },
    ])
    .then(({ name }) => {
      if (module === "List") {
        return chooseActions(name);
      } else if (module === "Redux") {
        return inputAction("Redux", name);
      }
      runner([module, "new", name]);
    });
};

const generateModule = () => {
  return inquirer
    .prompt([
      {
        type: "list",
        name: "action",
        message: "What would you like to do?",
        choices: ["Component", "Scene", "Redux", "List", "Quit"],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case "Component":
          return inputName("Component");
        case "Scene":
          return inputName("Scene");
        case "Redux":
          return inputName("Redux");
        case "List":
          return inputName("List");
        default:
          return process.exit();
      }
    });
};

const generateAuthFlow = (flowName) => {
  fs.pathExists("src/redux/reducers/authReducer.js").then((exist) => {
    if (!!!exist) {
      return inquirer
        .prompt([
          {
            type: "list",
            name: "action",
            message:
              "Redux for authentication is not existed. Do you want to add Redux for authentication?",
            choices: ["Yes", "No"],
          },
        ])
        .then(({ action }) => {
          switch (action) {
            case "Yes":
              return runner(["redux", "new", "auth"], () =>
                runner(["module", flowName])
              );
            case "No":
              return process.exit();
            default:
              return process.exit();
          }
        });
    } else {
      return runner(["module", flowName]);
    }
  });
};

const chooseSet = () => {
  return inquirer
    .prompt([
      {
        type: "list",
        name: "action",
        message: "What would you like to do?",
        choices: ["Redux", "SignIn", "SignUp", "Quit"],
      },
    ])
    .then(({ action }) => {
      switch (action) {
        case "Redux":
          return runner(["redux", "set"]);
        case "SignIn":
          return generateAuthFlow("signIn");
        case "SignUp":
          return generateAuthFlow("signUp");
        default:
          return process.exit();
      }
    });
};

const inputAction = (module, moduleName) => {
  inquirer
    .prompt([
      {
        type: "input",
        name: "name",
        message: "What is the action's name?",
        default: "name",
        validate: (input) => {
          return true;
        },
      },
    ])
    .then(({ name }) => {
      runner([module, "new", `--moduleName=${moduleName}`, `--name=${name}`]);
    });
};

const main = () => {
  switch (myArgs[0]) {
    case "module":
      return generateModule();
    case "set":
      return chooseSet();
    default:
      return;
  }
};

main();
