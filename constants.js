const AUTH_INJECTOR = [
    {
        below: '//Import Actions Type',
        path: './src/appRedux/actionTypes.js',
        value: `export const AUTH = {
    LOGIN: asyncTypes('AUTH/LOGIN'),
};`,
    },
    {
        below: 'combineReducers({',
        path: './src/appRedux/reducers/index.js',
        value: `auth: authReducer`
    },
    {
        below: '//Import Reducers',
        path: './src/appRedux/reducers/index.js',
        value: `import authReducer from './authReducer';`
    },
    {
        below: '//Import Sagas',
        path: './src/appRedux/sagas/index.js',
        value: `import authSagas from './authSagas';`
    },
    {
        below: '//Fork Sagas',
        path: './src/appRedux/sagas/index.js',
        value: `fork(authSagas),`
    }
]

module.exports = {
    AUTH_INJECTOR
}