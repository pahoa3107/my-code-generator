#!/usr/bin/env node


function inject(fs, path, below, input) {
    return new Promise((resolve) => {
        fs.readFile(path).then((content) => {
            const contentString = content.toString();
            const indexOf = contentString.lastIndexOf(below);
            const result = [contentString.slice(0, indexOf + below.length), `\n${input}`, contentString.slice(indexOf + below.length)].join('');
            fs.writeFile(path, result).then(() => resolve());
        });
    })
}

module.exports = {
    inject
}