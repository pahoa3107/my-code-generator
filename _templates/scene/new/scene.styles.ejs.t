---
to: src/scenes/main/<%= h.inflection.classify(name) %>/<%=h.inflection.classify(name)%>.styles.js
---
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
});
