---
to: src/scenes/main/<%= h.inflection.classify(name) %>/<%=h.inflection.classify(name)%>.view.js
---
<% formattedName = `${h.inflection.classify(name)}View` -%>
import React from 'react';
import {Text, SafeAreaView} from 'react-native';
import styles from './<%= h.inflection.classify(name) %>.styles.js'

const <%= formattedName %> = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Text><%= formattedName %></Text>
        </SafeAreaView>
    );
};

export default React.memo(<%= formattedName %>);
