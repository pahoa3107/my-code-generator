---
inject: true
to: src/routers/RootNavigator.js
skip_if: name={SCENE_NAMES.<%= h.inflection.underscore(name).toUpperCase() %>}
after: "Plop screen"
---
        <Stack.Screen
            options={{headerShown: false}}
            name={SCENE_NAMES.<%= h.inflection.underscore(name).toUpperCase() %>}
            component={<%= h.changeCase.pascal(name)%>Container}/>
