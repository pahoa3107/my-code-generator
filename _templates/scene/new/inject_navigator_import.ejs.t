---
inject: true
to: src/routers/RootNavigator.js
skip_if: <%= h.changeCase.pascal(name)%>Container
after: // Screen Import
---
import <%= h.changeCase.pascal(name)%>Container from '@Scenes/main/<%= h.changeCase.camel(name)%>/<%= h.changeCase.pascal(name)%>.container';
