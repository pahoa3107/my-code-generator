---
to: src/scenes/main/<%= h.inflection.classify(name) %>/<%=h.inflection.classify(name)%>.container.js
---
<% formattedName = `${h.inflection.classify(name)}Container` -%>
<% viewName = `${h.inflection.classify(name)}View` -%>

import React from 'react';
import <%= viewName %> from './<%= h.inflection.classify(name) %>.view.js';

export default function <%= formattedName %>() {
    return <<%=viewName %> />;
}

