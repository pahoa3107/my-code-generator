---
inject: true
to: src/constants/sceneName.js
after: "// Scene name"
---
<%= h.inflection.underscore(name).toUpperCase() %>: '<%= h.changeCase.pascal(name) %>Screen',