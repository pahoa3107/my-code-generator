---
to: src/redux/sagas/<%= `${h.changeCase.camel(name)}Sagas` %>/index.js
---
import {takeLatest} from 'redux-saga/effects';

export default function* <%=`${h.changeCase.camel(name)}Sagas`%>() {
    // Sagas
}