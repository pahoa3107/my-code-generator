---
inject: true
to: src/redux/sagas/index.js
skip_if: "fork(<%= `${h.changeCase.camel(name)}Sagas` %>)"
after: "// Sagas"
---
    fork(<%= `${h.changeCase.camel(name)}Sagas` %>),