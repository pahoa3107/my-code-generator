---
to: src/redux/reducers/<%= `${h.changeCase.camel(name)}Reducer.js` %>
---
<% formattedName = `${h.changeCase.camel(name)}Reducer` -%>
import {<%= h.inflection.underscore(name).toUpperCase() %>} from '@ActionsTypes';

const initialState = {};

const <%= formattedName %> = (state = initialState, action) => {
    switch (action.type) {
    // Reducers
        default:
            return state;
    }
};

export default <%= formattedName %>;
