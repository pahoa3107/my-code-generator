---
inject: true
to: src/redux/reducers/index.js
skip_if: "<%=h.changeCase.camel(name) %>: <%= `${h.changeCase.camel(name)}Reducer` %>"
after: "// Reducers"
---
<%=h.changeCase.camel(name) %>: <%= `${h.changeCase.camel(name)}Reducer` %>,