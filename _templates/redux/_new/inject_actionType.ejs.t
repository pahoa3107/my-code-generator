---
inject: true
to: src/redux/actionsType.js
skip_if: <%= h.inflection.underscore(name).toUpperCase() %>
after: "// Actions"
---
export const <%= h.inflection.underscore(name).toUpperCase() %> = {

};