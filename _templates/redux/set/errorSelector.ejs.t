---
to: src/redux/selectors/errorSelector.js
---
export const getErrorSelector = (state) => state.isError;
