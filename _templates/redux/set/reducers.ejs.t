---
to: src/redux/reducers/index.js
---
import {combineReducers} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';

// Reducer Imports
import errorReducer from './errorReducer';
import loadingReducer from './loadingReducer';
import connectReducer from './connectReducer';

// Persist Reducer

const rootReducer = combineReducers({
    // Reducers
    connect: connectReducer,
    error: errorReducer,
    loading: loadingReducer,
});

export default rootReducer;