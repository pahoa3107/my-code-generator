---
to: src/redux/actions/alertActions.js
---
import {ERROR} from '@ActionsTypes';

export const showError = payload => ({
  type: ERROR.DIALOG.SHOW,
  payload,
});

export const hideError = () => ({
  type: ERROR.DIALOG.HIDE,
});