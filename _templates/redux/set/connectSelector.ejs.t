---
to: src/redux/selectors/connectSelector.js
---
export const getIsConnectedSelector = (state) => state.connect.isConnected;
