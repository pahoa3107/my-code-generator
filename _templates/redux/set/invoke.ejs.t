---
to: src/helpers/sagas.js
---
import {put, select} from 'redux-saga/effects';
import {getIsConnectedSelector} from '@Selectors/connectSelector';
import {
  showLoading,
  onFetching,
  nonFetching,
  hideLoading,
} from '@Actions/loadingActions';
import {handleErrorMessage} from './handleError';
import {getString} from '@Utils/i18n';
import {showNotification} from '@Components/Modal/ModalNotification';

export function* invoke(execution, handleError, showDialog, actionType) {
  try {
    const isConnected = yield select(getIsConnectedSelector);
    if (!isConnected) {
      showNotification({
        message: getString('message.NOT_INTERNET'),
      });
      return;
    }
    if (showDialog) {
      yield put(showLoading(actionType));
    }
    yield put(onFetching(actionType));
    yield* execution();
    yield put(nonFetching(actionType));
    if (showDialog) {
      yield put(hideLoading(actionType));
    }
  } catch (error) {
    console.info(`Saga Invoke Error [${actionType}]>>>>>`, {error});
    yield put(nonFetching(actionType));
    if (showDialog) {
      yield put(hideLoading(actionType));
    }
    const errorMessage = handleErrorMessage(error);
    if (typeof handleError === 'function') {
      yield handleError(errorMessage);
    } else {
      // yield put(showError(errorMessage));
      yield showNotification({message: errorMessage?.message});
    }
  }
}