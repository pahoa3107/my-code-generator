---
inject: true
to: src/redux/<%= h.inflection.camelize(moduleName, true) -%>/reducer.js
skip_if: import
after: const
---
<% formattedName = h.inflection.camelize(moduleName, true) %>
<% formattedActionName = h.inflection.underscore(name).toUpperCase() -%>
import {<%= formattedActionName %>} from './constants';

const <%= formattedName %>Reducer = (state = initialState, action) => {
    switch (action?.type) {
        case <%= formattedActionName %>.SUCCESS: {
            return {...state, data: action?.payload};
        }
        default:
            return state;
    }
};

export default <%= formattedName %>Reducer;