---
to: src/redux/saga/rootSaga.js
---
import {all, fork} from 'redux-saga/effects';
import <%= h.inflection.camelize(moduleName, true) %>Saga from '../<%= h.inflection.camelize(moduleName, true) %>/sagas';

export default function* rootSaga() {
  yield all([
    fork(<%= h.inflection.camelize(moduleName, true) %>Saga),
  ]);
}