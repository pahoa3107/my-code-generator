---
to: src/redux/<%= h.inflection.camelize(moduleName, true) %>/constants.js
---
import {actionTypes} from '../actionTypes';