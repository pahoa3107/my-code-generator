---
to: src/redux/actionTypes.js
---
export const actionTypes = action => ({
  ORIGIN: action,
  HANDLER: `${action}_HANDLER`,
  PENDING: `${action}_PENDING`,
  START: `${action}_START`,
  MORE: `${action}_MORE`,
  SUCCESS: `${action}_SUCCESS`,
  FAILURE: `${action}_FAILURE`,
  ERROR: `${action}_ERROR`,
  CLEAR: `${action}_CLEAR`,
  END: `${action}_END`,
  GET: `${action}_GET`,
  ADD: `${action}_ADD`,
  EDIT: `${action}_EDIT`,
  REMOVE: `${action}_REMOVE`,
});