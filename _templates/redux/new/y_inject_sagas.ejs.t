---
inject: true
to: src/redux/<%= h.inflection.camelize(moduleName, true) -%>/sagas.js
skip_if: import {<%= formattedActionName %>} from './constants';
after: import {fork, put, takeLatest, takeEvery} from 'redux-saga/effects';
---
<% formattedActionName = h.inflection.underscore(name).toUpperCase() %>
import {
  <%= h.inflection.camelize(name, true) %>Success,
  <%= h.inflection.camelize(name, true) %>Failure
} from './actions';
import {<%= formattedActionName %>} from './constants';
import {
  <%= h.inflection.camelize(name, true) %>Api
} from '../../services/api/<%= h.inflection.camelize(moduleName, true) %>';

export function* <%= h.inflection.camelize(name, true) %>Saga(action) {
  const {payload, onSuccess, onError} = action;
  try {
    const res = yield callApi(<%= h.inflection.camelize(name, true) %>Api, payload);
    yield put(<%= h.inflection.camelize(name, true) %>Success(res?.data));
    onSuccess?.(res?.data);
  } catch (error) {
    yield put(<%= h.inflection.camelize(name, true) %>Failure(error));
    onError?.(error);
  }
}

<% formattedName = 'Watch' + name -%>

function* <%= h.inflection.camelize(formattedName, true) %>Saga() {
  yield takeLatest(
    <%= formattedActionName %>.HANDLER,
    <%= h.inflection.camelize(name, true) %>Saga,
  );
}