---
to: src/redux/<%= h.inflection.camelize(moduleName, true) %>/selector.js
inject: true
after:
skip_if: export
---
export const <%= h.inflection.camelize(name, true) %>Selector = (state) => state?.<%= h.inflection.camelize(moduleName, true) %>