---
to: src/redux/<%= h.inflection.camelize(moduleName, true) -%>/actions.js
inject: true
after: // empty actions
skip_if: import { <%= h.inflection.underscore(name).toUpperCase() %> } from './constants';
---
<% formattedName = h.inflection.underscore(name).toUpperCase() %>
import { <%= h.inflection.underscore(name).toUpperCase() %> } from './constants';

export const <%= h.inflection.camelize(name, true) %>Handle = (onSuccess, onFailed) => ({
  type: <%= formattedName %>.HANDLER,
  onSuccess,
  onFailed,
});

export const <%= h.inflection.camelize(name, true) %>Success = payload => ({
  type: <%= formattedName %>.SUCCESS,
  payload,
});

export const <%= h.inflection.camelize(name, true) %>Failure = error => ({
  type: <%= formattedName %>.FAILURE,
  error,
});