---
to: src/redux/reducer/rootReducer.js
---
import {combineReducers} from 'redux';
import {persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import <%= h.inflection.camelize(moduleName, true) %>Reducer from '../<%= h.inflection.camelize(moduleName, true) %>/reducer';

const <%= h.inflection.camelize(moduleName, true) %>PersistConfig = {
  key: '<%= h.inflection.camelize(moduleName, true) %>',
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
 <%= h.inflection.camelize(moduleName, true) %>: persistReducer(<%= h.inflection.camelize(moduleName, true) %>PersistConfig, <%= h.inflection.camelize(moduleName, true) %>Reducer),
});

export default rootReducer;
