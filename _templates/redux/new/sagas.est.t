---
to: src/redux/<%= h.inflection.camelize(moduleName, true) %>/sagas.js
---
import {callApi} from '../../helpers/callApi';
import {fork, put, takeLatest, takeEvery} from 'redux-saga/effects';

export default function* rootChild() {
  yield fork(watch<%= h.inflection.classify(name) %>Saga);
}