---
to: src/redux/<%= h.inflection.camelize(moduleName, true) -%>/constants.js
inject: true
after: import {actionTypes} from '../actionTypes';
skip_if: export
---
<% formattedName = h.inflection.underscore(name).toUpperCase() %>
export const <%= formattedName %> = actionTypes('<%= h.inflection.camelize(moduleName, true) -%>/<%= formattedName %>');