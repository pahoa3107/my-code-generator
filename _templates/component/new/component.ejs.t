---
to: src/components/<%= h.inflection.classify(name) %>/index.js
---
<% formattedName = h.inflection.classify(name) -%>
import React from 'react';
import { View } from 'react-native';
import styles from './styles';

const <%= formattedName %> = () => <View style={styles.container} />;

export default React.memo(<%= formattedName %>);
