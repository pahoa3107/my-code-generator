---
to: src/components/<%= h.inflection.classify(name) %>/styles.js
---
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
    },
});

