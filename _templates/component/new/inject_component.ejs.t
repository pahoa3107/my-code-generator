---
inject: true
to: src/constants/Colors.js
skip_if: <%= name %>
after: ""
---
<%= Name %>: require('../components/<%= name %>').default,

