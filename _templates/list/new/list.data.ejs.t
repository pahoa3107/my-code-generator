---
to: src/helpers/globalData.js
---
global.dataExample = {
  data: [
    {
      id: 1,
      name: 'Foundation',
      desc: 'Lấy lại căng bản và giao tiếp',
      image: 'https://testoffice.talksenglish.com/upload/level/level-1.png',
    },
    {
      id: 2,
      name: 'Basic communication',
      desc: 'Luyện tập giao tiếp chủ đề cơ bản',
      image: 'https://testoffice.talksenglish.com/upload/level/level-2.png',
    },
    {
      id: 3,
      name: 'Developing communication',
      desc: 'Luyên tập giao tiếp chủ đề chuyên sâu',
      image: 'https://testoffice.talksenglish.com/upload/level/level-3.png',
    },
    {
      id: 4,
      name: 'Debate-Discussion',
      desc: 'Lập luận tư duy phản biện',
      image: 'https://testoffice.talksenglish.com/upload/level/level-4.png',
    },
    {
      id: 5,
      name: 'Talks Online',
      desc: 'Một kèm một',
      image:
        'https://testoffice.talksenglish.com/upload/level/level-default.png',
    },
  ],
  total: 10,
};