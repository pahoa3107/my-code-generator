---
to: src/redux/parsers/<%= h.changeCase.camel(name) %>Parsers.js
---
import {PaginationData, PaginationInfo} from '@Models';
<% formattedName = h.changeCase.pascal(name) -%>
export const parse<%= formattedName %>Item = (item) => {
    return {
        <% fields.map((item) => {%><%= item.value %>: item?.<%= item.value %>,
        <%})%>
    }
};

export const parse<%= formattedName %>List = (res, currentPage, perPage) => {
    const {data, total} = res || {};
    let newData = [];
    if (Array.isArray(data)) {
        newData = data.map((e) => {
        return parse<%= formattedName %>Item(e);
        });
    }
    return new PaginationData({
        data: newData,
        pagination: new PaginationInfo({currentPage, totalPage: Math.ceil(total / perPage)}),
    });
};