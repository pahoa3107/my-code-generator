---
inject: true
to: src/redux/reducers/index.js
skip_if: import <%= `${h.changeCase.camel(name)}Reducer` %>
after: "// Reducer Imports"
---
import <%= `${h.changeCase.camel(name)}Reducer` %> from './<%= `${name.toLowerCase()}Reducer` %>';