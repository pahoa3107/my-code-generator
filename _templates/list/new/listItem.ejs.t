---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>List/components/<%= h.changeCase.pascal(name) %>Item/index.js
---
<% pascalName = h.changeCase.pascal(name) -%>
import React, {useCallback} from 'react'
import styles from './styles';
import { TouchableOpacity } from 'react-native';
import AppText from '@Components/AppText';
import AppImage from '@Components/AppImage';

const <%= pascalName %>Item = ({ item, onPress }) => {
    const { image, name } = item || {};
    const _onPress = useCallback(() => {
        typeof onPress === 'function' && onPress(item);
    }, [item, onPress]);
    return (
        <TouchableOpacity style={styles.container} onPress={_onPress}>
            <AppImage url={image} style={styles.image} />
            <AppText bold small style={styles.nameText} numberOfLines={2}>
                {name}
            </AppText>
        </TouchableOpacity>
    );
};

export default React.memo(<%= pascalName %>Item);
