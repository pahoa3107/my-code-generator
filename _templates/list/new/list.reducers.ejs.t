---
to: src/redux/reducers/<%= h.changeCase.camel(name) %>Reducer.js
---
<% actionTypeName = h.inflection.underscore(name).toUpperCase() -%>
<% pascalName = h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(name) -%>
import {<%= actionTypeName %>} from '@ActionsTypes';
import {PaginationData} from '@Models';

const initialState = {
    list<%= pascalName %>: new PaginationData(),
};

// Handler

const <%= camelName %>Reducer = (state = initialState, action) => {
    switch (action.type) {
    // Reducers
        case <%= actionTypeName %>.GET_LIST.SUCCESS: {
            return {
                ...state,
                list<%= pascalName %>: action.payload
            }
        }
        default:
            return state;
    }
};

export default <%= camelName %>Reducer;