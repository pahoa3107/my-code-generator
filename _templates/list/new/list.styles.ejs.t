---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>List/<%= h.changeCase.pascal(name) %>List.styles.js
---
import {DEFAULT_PADDING_VERTICAL} from '@Constants/size';
import {scalePortrait} from '@Utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  safeView: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingHorizontal: scalePortrait(30),
    paddingTop: DEFAULT_PADDING_VERTICAL,
  },
  list: {},
  contentContainer: {
    alignItems: 'center',
  },
  title: {
    fontSize: scalePortrait(24),
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingBottom: scalePortrait(10),
  },
  addBtn: {
    backgroundColor: '#0EAC51',
  },
  addIcon: {
    fontSize: scalePortrait(25),
  },
});