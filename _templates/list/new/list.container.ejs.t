---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>List/<%= h.changeCase.pascal(name) %>List.container.js
---
<% formattedName = h.changeCase.pascal(name) -%>
import React, {useCallback, useEffect} from 'react';
import <%= formattedName %>ListView from './<%= formattedName %>List.view'; 
import useSelectorShallow from '@Hooks/useSelectorShallowEqual'; 
import {useActions} from '@Hooks/useActions';
import {get<%= formattedName %>ListSubmit} from '@Redux/actions/<%= h.changeCase.camel(name) %>Actions';
import {get<%= formattedName %>ListSelector} from '@Redux/selectors/<%= h.changeCase.camel(name) %>Selector';
import NavigationServices from '@Utils/navigationServices';
import SCENE_NAMES from '@Constants/sceneName';
import {DeviceEventEmitter} from 'react-native';

export default function <%= formattedName %>ListContainer() {
    const actions = useActions({get<%= formattedName %>ListSubmit});
    const list<%= formattedName %> = useSelectorShallow(get<%= formattedName %>ListSelector);

    const onFetch = useCallback(
    (page) => {
      actions.get<%= formattedName %>ListSubmit({
        params: {
          page,
          perPage: 5,
        },
      });
    },
    [actions],
   );

   const onRefresh = useCallback(() => {
    onFetch(1);
   }, [onFetch]);

   useEffect(() => {
    onFetch(1);
   }, [onFetch]);

   useEffect(() => {
    const listener = DeviceEventEmitter.addListener('FETCH_LIST', () =>
      onRefresh(),
    );
    return () => listener.remove();
   }, [onRefresh]);

   const onPressItem = useCallback((item) => {
     NavigationServices.navigate(SCENE_NAMES.<%= h.inflection.underscore(name).toUpperCase() %>_DETAIL, {<%= h.changeCase.camel(name) %>Detail: item})
   }, [])
   <%if(actions.includes('Add')){%>
    const onPressAddItem = useCallback(() => {
      NavigationServices.navigate(SCENE_NAMES.ADD_<%= h.inflection.underscore(name).toUpperCase() %>);
    }, []);
   <%}%>

    return <<%= formattedName %>ListView dataPaging={list<%= formattedName %>} 
                onRefresh={onRefresh}
                onFetch={onFetch}
                onPressItem={onPressItem} 
                <%if(actions.includes('Add')){%>
                onPressAddItem={onPressAddItem}
                <%}%>
            />
}