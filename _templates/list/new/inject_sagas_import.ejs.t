---
inject: true
to: src/redux/sagas/index.js
skip_if: ./<%= `${h.changeCase.camel(name)}Sagas` %>
after: "// Saga Imports"
---
import <%= `${h.changeCase.camel(name)}Sagas` %> from './<%= `${h.changeCase.camel(name)}Sagas` %>';