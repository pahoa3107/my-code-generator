---
inject: true
to: src/routers/RootNavigator.js
skip_if: <%= h.changeCase.pascal(name)%>ListContainer
after: // Screen Import
---
import <%= h.changeCase.pascal(name)%>ListContainer from '@Scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>List/<%= h.changeCase.pascal(name) %>List.container';
