---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>List/<%= h.changeCase.pascal(name) %>List.view.js
---
<% formattedName = h.changeCase.pascal(name) -%>
import React, {useCallback} from 'react';
import {View, SafeAreaView} from 'react-native';
import styles from './<%= formattedName %>List.styles';
import PaginationList from '@Components/PaginationList';
import AppText from '@Components/AppText';
import <%= formattedName %>Item from './components/<%= formattedName %>Item';
import {Button} from 'native-base';
import {AppIcon} from '@Components';

function <%= formattedName %>ListView({isFetching, dataPaging, onRefresh, onFetch, onPressItem, onPressAddItem}) {
    const renderItem = useCallback(({item, index}) => {
        return <<%= formattedName %>Item item={item} onPress={onPressItem}/>
    }, [onPressItem]);

    return (
            <SafeAreaView style={styles.safeView}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <AppText bold style={styles.title}>
                            <%= formattedName %> List
                        </AppText>
                        <% if(actions.includes('Add')){%><Button style={styles.addBtn} onPress={onPressAddItem}>
                            <AppIcon type="Entypo" name="plus" style={styles.addIcon} />
                        </Button><%}%>
                    </View>
                    <PaginationList 
                        style={styles.list}
                        contentContainerStyle={styles.contentContainer}
                        //isFetching={isFetching}
                        dataPaging={dataPaging}
                        onRefresh={onRefresh}
                        onFetch={onFetch}
                        renderItem={renderItem}
                    />
                </View>
            </SafeAreaView>
    );
        
};

export default React.memo(<%= formattedName %>ListView);