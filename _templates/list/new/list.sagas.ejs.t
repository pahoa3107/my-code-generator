---
to: src/redux/sagas/<%= h.changeCase.camel(name) %>Sagas/index.js
---
<% formattedName = h.changeCase.pascal(name) -%>
import {takeLatest} from 'redux-saga/effects';
import {get<%= formattedName %>ListSaga} from './get<%= formattedName %>ListSaga';
import {<%= h.inflection.underscore(name).toUpperCase() %>} from '@ActionsTypes';
// Sagas Import

export default function* <%= h.changeCase.camel(name) %>Sagas() {
  yield takeLatest(<%= h.inflection.underscore(name).toUpperCase() %>.GET_LIST.HANDLER, get<%= formattedName %>ListSaga);
}