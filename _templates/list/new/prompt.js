const requiredFields = [
  {
    value: "id",
    type: "string",
  },
  {
    value: "name",
    type: "string",
  },
  {
    value: "image",
    type: "string",
  },
  {
    value: "desc",
    type: "string",
  },
];
module.exports = {
  prompt: ({ prompter }) =>
    prompter
      .prompt([
        {
          type: "input",
          name: "fields",
          message: "Please enter fields (exclude: id, name, image, desc).",
        },
      ])
      .then(({ fields }) => {
        let result = [];
        if (fields) {
          const arrFields = fields.split(" ");
          const fieldSplit = arrFields.map((item) => {
            const [value, type] = item.split(":");
            return { value, type };
          });
          result = [...requiredFields, ...fieldSplit];
        } else {
          result = [...requiredFields];
        }

        global.fields = result;
        return { fields: result };
      }),
};
