---
to: src/services/apis/<%= h.changeCase.camel(name) %>Api.js
---
<% pascalName = h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(name) -%>
import utils from '@Utils/apiUtils';
import AppConfigs from '@Configs/appConfigs';
import '@Helpers/globalData';

const END_POINT = AppConfigs.END_POINT;

// Apis

export const get<%= pascalName %>ListApi = (params) => {
    const {page = 1, perPage = 10} = params || {};
    //return utils.get(`${END_POINT}/<%= camelName %>?page=${page}&perPage=${perPage}`,);
    return global.dataExample;
}