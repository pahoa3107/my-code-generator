---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>List/components/<%= h.changeCase.pascal(name) %>Item/styles.js
---
import {
  DEFAULT_PADDING_VERTICAL,
  DEFAULT_SPACING_HORIZONTAL,
} from '@Constants/size';
import {StyleSheet} from 'react-native';
import {scalePortrait} from '@Utils/responsive';

export default StyleSheet.create({
  container: {
    width: scalePortrait(310),
    borderRadius: scalePortrait(10),
    shadowColor: 'rgba(0,0,0,0.15)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    marginBottom: DEFAULT_PADDING_VERTICAL,
    marginHorizontal: DEFAULT_SPACING_HORIZONTAL,
    backgroundColor: '#E0E0E0',
    paddingBottom: scalePortrait(15),
  },
  image: {
    width: '100%',
    height: scalePortrait(190),
    overflow: 'hidden',
    borderTopLeftRadius: scalePortrait(10),
    borderTopRightRadius: scalePortrait(10),
  },
  nameText: {
    marginTop: DEFAULT_PADDING_VERTICAL / 2,
    marginHorizontal: DEFAULT_PADDING_VERTICAL / 2,
  },
});
