---
inject: true
to: src/constants/sceneName.js
after: "// Scene name"
skip_if: <%= h.inflection.underscore(name).toUpperCase() %>_LIST
---
<%= h.inflection.underscore(name).toUpperCase() %>_LIST: '<%= h.changeCase.pascal(name) %>ListScreen',