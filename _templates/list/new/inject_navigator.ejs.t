---
inject: true
to: src/routers/RootNavigator.js
skip_if: name={SCENE_NAMES.<%= h.inflection.underscore(name).toUpperCase() %>_LIST}
after: "Plop screen"
---
        <Stack.Screen
            options={{headerShown: false}}
            name={SCENE_NAMES.<%= h.inflection.underscore(name).toUpperCase() %>_LIST}
            component={<%= h.changeCase.pascal(name)%>ListContainer}/>
