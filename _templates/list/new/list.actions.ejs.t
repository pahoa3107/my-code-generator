---
to: src/redux/actions/<%= h.changeCase.camel(name) %>Actions.js
---
<% formattedName = h.changeCase.pascal(name) -%>
<% typeName = name.toUpperCase() -%>
import {<%= typeName %>} from '@ActionsTypes';

// Actions 

export const get<%= formattedName %>ListSubmit = (payload) => ({
    type: <%= typeName %>.GET_LIST.HANDLER,
    payload,
});

export const get<%= formattedName %>ListSuccess = (payload) => ({
    type: <%= typeName %>.GET_LIST.SUCCESS,
    payload,
});