---
to: src/redux/sagas/<%= h.changeCase.camel(name) %>Sagas/get<%= h.changeCase.pascal(name) %>ListSaga.js
---
<% formattedName = h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(name) -%>
import {put, call, select} from 'redux-saga/effects';
import {get<%= formattedName %>ListSuccess} from '@Redux/actions/<%= camelName %>Actions';
import {get<%= formattedName %>ListApi} from '@Services/apis/<%= camelName %>Api';
import {invoke} from '@Helpers/sagas';
import {parse<%= formattedName %>List} from '@Redux/parsers/<%= camelName %>Parsers';
import {pushPagingData} from '@Helpers/pagings';
import {get<%= formattedName %>ListSelector} from '@Redux/selectors/<%= camelName %>Selector';

export function* get<%= h.changeCase.pascal(name) %>ListSaga({payload, type}) {
  const {showLoading = true, callback = () => {}, params} = payload || {};
  yield invoke(
    function* execution() {
      const {page, perPage} = params || {};
      const result = yield call(get<%= formattedName %>ListApi, params);
      const dataParse = parse<%= formattedName %>List(result, page, perPage);
      let newData = dataParse;
      if (page > 1) {
        const dataSource = yield select(get<%= formattedName %>ListSelector);
        newData = pushPagingData(dataSource, dataParse);
      }
      yield put(get<%= formattedName %>ListSuccess(newData));
      yield callback(null, dataParse);
    },
    null,
    showLoading,
    type,
  );
}
