---
inject: true
to: src/redux/actionsType.js
skip_if: <%= name.toUpperCase() %>
after: "// Actions"
---
<% formattedName = h.inflection.underscore(name).toUpperCase() -%>
export const <%= formattedName %> = {
    GET_LIST: asyncTypes('<%= formattedName %>/GET_LIST'),
};