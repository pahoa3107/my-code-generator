---
to: src/redux/selectors/<%= h.changeCase.camel(name) %>Selector.js
---
<% formattedName = h.changeCase.pascal(name) -%>
export const get<%= formattedName %>ListSelector = (state) => state.<%= h.changeCase.camel(name) %>.list<%= formattedName %>;