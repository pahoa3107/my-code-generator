---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>Detail/<%= h.changeCase.pascal(name) %>Detail.container.js
---
<% pascalName = h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(name) -%>
import React, {useCallback} from 'react';
import <%= pascalName %>DetailView from './<%= pascalName %>Detail.view';
import NavigationServices, {getParams} from '@Utils/navigationServices';
import {showNotification} from '@Components/Modal/ModalNotification';
import SCENE_NAMES from '@Constants/sceneName';

export default function <%= pascalName %>DetailContainer({route}) {
    const {<%= camelName %>Detail} = getParams(route);
    <% if(actions.includes('Update') || actions.includes('Delete')){%>
    const onPressActionBtn = useCallback(() => {
        showNotification({
        <%if(actions.includes('Update')){%>textConfirm: 'Update',
        hasConfirm: true,
        onPressConfirm: () => {
            NavigationServices.navigate(SCENE_NAMES.UPDATE_<%= h.inflection.underscore(name).toUpperCase()%>, {
            <%= camelName %>Detail,
            });
        },<%}else{%>hasConfirm: false,<%}%>
        <%if(actions.includes('Update')){%>textCancel: 'Delete',
        textCancel: 'Delete',
        hasCancel: true,
        <%}else{%>hasCancel: false,<%}%>
        hasClose: true,
        message: null,
        title: 'Choose action',
        });
    }, [<%= camelName %>Detail]);
    <%}%>
    return (
        <<%= pascalName %>DetailView 
            <%= camelName %>Detail={<%= camelName %>Detail} 
            onPressActionBtn={onPressActionBtn}
            <% if(actions.includes('Update') || actions.includes('Delete')){%>
            onPressActionBtn={onPressActionBtn}
            <%}%>
        />
    )
};
