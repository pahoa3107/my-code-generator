---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>Detail/<%= h.changeCase.pascal(name) %>Detail.view.js
---
<% pascalName = h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(name) -%>
<% dateValue = fields.filter((item) => item.type === 'date') -%> 
<% numValue = fields.filter((item) => item.type === 'number') -%>
import React from 'react'
import {ScrollView, ImageBackground, TouchableOpacity} from 'react-native'
import styles from './<%= pascalName %>Detail.styles';
import {AppText, AppIcon} from '@Components';
import NavigationServices from '@Utils/navigationServices';
import moment from 'moment';

const <%= pascalName %>DetailView = ({<%= camelName %>Detail, onPressActionBtn}) => {
    const {<%fields.map((item) => {if(item.value !== 'id'){%><%=item.value%>, <%}})%>} = <%= camelName %>Detail || {};
    return (
        <>
            <TouchableOpacity
                style={styles.backBtn}
                onPress={() => {
                NavigationServices.goBack();
                }}>
                <AppIcon type="Ionicons" name="chevron-back" />
            </TouchableOpacity>
            <% if(actions.includes('Update') || actions.includes('Delete')){%>
            <TouchableOpacity style={styles.actionBtn} onPress={onPressActionBtn}>
                <AppIcon type="Entypo" name="dots-three-horizontal" />
            </TouchableOpacity>
            <%}%>
            <ImageBackground source={{uri: image}} style={styles.bgImg} />
            <ScrollView style={styles.container} bounces={false}>
                <%if(dateValue.length !== 0){dateValue.map((item) => {%><AppText style={styles.date}>
                    <%=item.value%>: {moment(<%=item.value%>).format('DD/MM/YYYY')}
                </AppText><%})}%>
                <AppText large bold style={styles.title}>
                    {name}
                </AppText>
                <%if(numValue.length !== 0){numValue.map((item) => {%><AppText fit bold style={styles.number}>
                    <%=item.value%>: {<%=item.value%>}
                </AppText><%})}%>
                <%fields.map((item) => {if(!['name', 'desc', 'image', 'id'].includes(item.value) && !['date', 'number'].includes(item.type)){%><AppText normal regular>
                    {<%=item.value%>}
                </AppText><%}})%>
                <AppText normal regular>
                    {desc}
                </AppText>
            </ScrollView>
        </>
    );
};

export default React.memo(<%= pascalName %>DetailView);