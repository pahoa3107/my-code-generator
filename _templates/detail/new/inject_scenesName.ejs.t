---
inject: true
to: src/constants/sceneName.js
after: "// Scene name"
skip_if: <%= h.inflection.underscore(name).toUpperCase() %>_DETAIL
---
<%= h.inflection.underscore(name).toUpperCase() %>_DETAIL: '<%= h.changeCase.pascal(name) %>DetailScreen',