---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>Detail/<%= h.changeCase.pascal(name) %>Detail.styles.js
---
import {StyleSheet} from 'react-native';
import {
  INITIAL_WINDOW_SAFE_AREA_INSETS,
  scalePortrait,
} from '@Utils/responsive';
import {
  DEFAULT_PADDING_HORIZONTAL,
  DEFAULT_PADDING_VERTICAL,
} from '@Constants/size';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: scalePortrait(10),
  },
  bgImg: {
    height: scalePortrait(230),
  },
  backBtn: {
    position: 'absolute',
    zIndex: 9999,
    left: DEFAULT_PADDING_HORIZONTAL,
    top: DEFAULT_PADDING_VERTICAL + INITIAL_WINDOW_SAFE_AREA_INSETS.top,
    height: scalePortrait(40),
    width: scalePortrait(40),
    backgroundColor: 'rgba(210, 210, 210, 0.3)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scalePortrait(40),
  },
  actionBtn: {
    position: 'absolute',
    zIndex: 9999,
    right: DEFAULT_PADDING_HORIZONTAL,
    top: DEFAULT_PADDING_VERTICAL + INITIAL_WINDOW_SAFE_AREA_INSETS.top,
    height: scalePortrait(40),
    width: scalePortrait(40),
    backgroundColor: 'rgba(210, 210, 210, 0.3)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: scalePortrait(40),
  },
  title: {
    paddingBottom: scalePortrait(5),
  },
  number: {
    color: '#22A7F0',
    paddingBottom: scalePortrait(5),
  },
  date: {
    color: '#A0A0A0',
    paddingBottom: scalePortrait(5),
  },
});
