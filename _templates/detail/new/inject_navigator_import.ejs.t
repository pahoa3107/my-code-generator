---
inject: true
to: src/routers/RootNavigator.js
skip_if: <%= h.changeCase.pascal(name)%>DetailContainer
after: // Screen Import
---
import <%= h.changeCase.pascal(name)%>DetailContainer from '@Scenes/main/<%= h.changeCase.camel(name) %>/<%= h.changeCase.camel(name) %>Detail/<%= h.changeCase.pascal(name) %>Detail.container';
