---
to: src/scenes/auth/signUp/SignUp.constants.js
---
export const NAMESPACE = 'scenes.auth.signUp';
import * as Yup from 'yup';

export const SIGN_UP_SCHEME = Yup.object().shape({
    <% fields.map((field) => {%><%= h.changeCase.camelCase(field.name) %>: Yup.string().required('<%= field.name %> is required.'),
    <%})%>
});
