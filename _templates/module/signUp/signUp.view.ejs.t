---
to: src/scenes/auth/signUp/SignUp.view.js
---
import { useFormik } from 'formik';
import React, { forwardRef, useImperativeHandle } from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text, TextInput } from 'react-native';
import { SIGN_UP_SCHEME } from './SignUp.constants';
import styles from './SignUp.styles';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const Input = React.memo(({isNumer, placeholder, isSecure, value, onChangeText, error, editable = true }) => {
    return (
        <View>
            <TextInput keyboardType={isNumer ? "numeric" : "default"} onChangeText={onChangeText} value={value} secureTextEntry={isSecure} placeholder={placeholder} style={styles.textInput} placeholderTextColor="rgba(34, 167, 240, 0.4)" editable={editable} pointerEvents="none" />
            {error && <Text style={styles.txtError}>{error}</Text>}
        </View>
    );
});

function SignUpView({ onSignUp, onConfirmDatePicker, onCancelDatePicker, date, onPressDOB, dateTimePickerVisible }, ref) {
    const { handleChange, values, handleSubmit, errors, setFieldValue} = useFormik({ initialValues: {<%fields.forEach(function(field){%> <%= h.changeCase.camelCase(field.name) %>: '',<%})%>}, validationSchema: SIGN_UP_SCHEME, onSubmit: onSignUp, validateOnChange: false});
    useImperativeHandle(
        ref,
        () => {
            return { setFieldValue };
        },
    );
    return (
        <View style={styles.safeView}>
            <Text style={styles.title}>SIGN UP</Text>
            <KeyboardAwareScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <View style={styles.break} /><%fields.forEach(function(field){ %>
                    <%if(field.type !== 'date'){%><Input isNumer={<%= field.type === 'number' %>} isSecure={<% if(field.name === 'Password' || field.name === 'Confirm Password'){%>true<%}else{%>false<%}%>} placeholder="<%= field.name %>" value={values.<%= h.changeCase.camelCase(field.name) %>} onChangeText={handleChange('<%= h.changeCase.camelCase(field.name) %>')} error={errors.<%= h.changeCase.camelCase(field.name) %>} /><%}else{%><TouchableOpacity style={styles.dob} onPress={onPressDOB}>
                        <Input editable={false} placeholder="<%= field.name %>" value={values.<%= h.changeCase.camelCase(field.name) %>} onChangeText={handleChange('<%= h.changeCase.camelCase(field.name) %>')} error={errors.<%= h.changeCase.camelCase(field.name) %>} />
                    </TouchableOpacity>
                    <DateTimePickerModal
                        date={date}
                        onCancel={onCancelDatePicker}
                        onConfirm={onConfirmDatePicker}
                        mode="date"
                        isVisible={dateTimePickerVisible}/><%}%><View style={styles.break} /><% })%><TouchableOpacity style={styles.signUpBtn} onPress={handleSubmit}>
                        <Text style={styles.btnLabel}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        </View>
    );
}

export default React.memo(forwardRef(SignUpView));