---
inject: true
to: src/services/apis/authApi.js
skip_if: signUpApi
after: // APIS
--- 
export const signUpApi = (params) => {
  return utils.post(`${END_POINT}/signUp`, params);
};