---
to: src/redux/sagas/authSagas/signUpSaga.js
---
import {put, call} from 'redux-saga/effects';
import {signUpApi} from '@Apis/authApi';
import {invoke} from '@Helpers/sagas';

export function* signUpSaga({payload, type}) {
  const {showLoading = true, callback = () => {}, params} = payload || {};
  yield invoke(
    function* execution() {
      console.log("Params>>>>> ", params)
      const result = yield call(signUpApi, params);
      yield callback(null, result);
    },
    null,
    showLoading,
    type,
  );
}