---
to: src/scenes/auth/signUp/SignUp.container.js
---
<% hasDOB = fields.filter((field) => field.name === 'Date Of Birth').length > 0 -%>
import React, {useCallback, <%if(hasDOB) {%>useState, useRef<%}%>} from 'react';
import SignUpView from './SignUp.view';
import { signUpSubmit } from '@Redux/actions/authActions';
import { useActions } from '@Hooks/useActions';
<%if(hasDOB) {%>import moment from 'moment';<%}%>

export default function SignUpContainer() {
    <% if(hasDOB) {%>
    const [dateTimePickerVisible, setDateTimePickerVisible] = useState(false);
    const [date, setDate] = useState(new Date());
    const ref = useRef();
    const actions = useActions({ signUpSubmit });

    const onCancelDatePicker = useCallback(() => {
        setDateTimePickerVisible(false);
    }, []);

    const onConfirmDatePicker = useCallback((selectedDate) => {
        setDateTimePickerVisible(false);
        setDate(selectedDate);
        ref.current.setFieldValue('dateOfBirth', moment(selectedDate).format('DD-MM-YYYY'));
    }, []);

    const onPressDOB = () => {
        setDateTimePickerVisible(true);
    };
    <%}%>

    const onSignUp = useCallback((value) => {
        actions.signUpSubmit({
                params: value,
            });
    }, [actions]);

    return (
        <SignUpView 
            onSignUp={onSignUp}<%if(hasDOB) {%>
            ref={ref} 
            dateTimePickerVisible={dateTimePickerVisible} 
            date={date} 
            onCancelDatePicker={onCancelDatePicker} 
            onConfirmDatePicker={onConfirmDatePicker} 
            onPressDOB={onPressDOB}<%}%>
        />
    );
};