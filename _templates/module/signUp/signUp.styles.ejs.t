---
to: src/scenes/auth/signUp/SignUp.styles.js
---
import { StyleSheet } from 'react-native';
export const MAIN_COLOR = '#22A7F0';

export default StyleSheet.create({
    safeView: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 30,
        paddingTop: 70,
    },
    scrollView: {
        flex: 1,
        backgroundColor: '#fff',
    },
    container: {
        paddingBottom: 30,
        flex: 1,
    },
    textInput: {
        width: '100%',
        borderWidth: 1,
        borderColor: MAIN_COLOR,
        color: MAIN_COLOR,
        paddingLeft: 10,
        height: 50,
    },
    signUpBtn: {
        padding: 10,
        backgroundColor: MAIN_COLOR,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputContainer: {
        width: '100%',
    },
    break: {
        height: 30,
    },
    btnLabel: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 17,
    },
    title: {
        fontSize: 31,
        fontWeight: 'bold',
        color: MAIN_COLOR,
    },
    txtError: {
        color: '#D32F2F',
        alignSelf: 'flex-start',
    },
    dob: {
        width: '100%',
    },
});
