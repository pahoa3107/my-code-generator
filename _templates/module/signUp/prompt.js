const CHOICES = [
    {
        name: 'Full Name',
        type: 'string'
    },
    {
        name: 'Date Of Birth',
        type: 'date'
    },
    {
        name: 'Address',
        type: 'string'
    }
]

module.exports = {
    prompt: ({ prompter }) =>
        prompter
            .prompt([
                {
                    type: 'select',
                    name: 'type',
                    message: 'What type of username?',
                    choices: ['Username', 'Email', 'Phone Number']
                },
            ])
            .then(({ type }) => {
                let _choices = [];
                const insert = (array = [], index, value) => {
                    if (Array.isArray(value)) {
                        return [...array.slice(0, index), ...value, ...array.slice(index)];
                    }
                    return [...array.slice(0, index), value, ...array.slice(index)];
                }
                switch (type) {
                    case 'Username':
                        _choices = insert(CHOICES, 1, [{ name: 'Email', type: 'string' }, { name: 'Phone Number', type: 'number' }]);
                        break;
                    case 'Email':
                        _choices = insert(CHOICES, 1, { name: 'Phone Number', type: 'string' });
                        break;
                    case 'Phone Number':
                        _choices = insert(CHOICES, 1, { name: 'Email', type: 'string' });
                    default:
                        break;
                }
                return prompter
                    .prompt([
                        {
                            type: 'multiselect',
                            name: 'fields',
                            message: 'Please select fields. Press space key to select and end with enter key',
                            choices: _choices.map((item) => item.name),
                            initial: []
                        },
                    ]).then(({ fields }) => {
                        let _fields = [];
                        fields.forEach((field) => {
                            _choices.forEach((choice) => {
                                if (choice.name === field) {
                                    _fields.push(choice)
                                }
                            })
                        })
                        let fieldSelected = [];
                        if (fields.includes('Full Name')) {
                            fieldSelected = insert(_fields, 1, [{ name: type, type: 'string' }, { name: 'Password', type: 'string' }, { name: 'Confirm Password', type: 'string' }]);
                        } else {
                            fieldSelected = [...[{ name: type, type: 'string' }, { name: 'Password', type: 'string' }, { name: 'Confirm Password', type: 'string' }], ..._fields];
                        }
                        return { "fields": fieldSelected };
                    })
            })
}



