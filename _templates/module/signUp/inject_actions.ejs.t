---
inject: true
to: src/redux/actions/authActions.js
skip_if: signUpSubmit
after: // Actions
--- 
export const signUpSubmit = (payload) => ({
    type: AUTH.SIGN_UP.HANDLER,
    payload,
});

export const signUpSuccess = (payload) => ({
    type: AUTH.SIGN_UP.SUCCESS,
    payload,
});