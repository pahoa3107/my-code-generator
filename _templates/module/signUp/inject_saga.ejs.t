---
inject: true
to: src/redux/sagas/authSagas/index.js
skip_if: AUTH.SIGN_UP.HANDLER
after: "export default function*"
---
    yield takeLatest(AUTH.SIGN_UP.HANDLER, signUpSaga);