---
inject: true
to: src/redux/sagas/authSagas/index.js
skip_if: ./signUpSaga
after: ""
---
import {signUpSaga} from './signUpSaga';