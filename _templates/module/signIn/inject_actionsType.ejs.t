---
inject: true
to: src/redux/actionsType.js
skip_if: LOGIN
after: "export const AUTH = {"
--- 
    LOGIN: asyncTypes('AUTH/SIGN_IN'),
    ACCESS_TOKEN: 'AUTH/ACCESS_TOKEN',