---
to: src/scenes/auth/signIn/SignIn.constants.js
---
export const NAMESPACE = 'scenes.auth.signIn';
import * as Yup from 'yup';

export const SIGN_IN_SCHEME = Yup.object().shape({
    username: Yup.string().matches(/^[0][0-9]{9,9}\b|^[1-9][0-9]{8,8}\b|^[a-z][a-z0-9]{4,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/, 'Invalid email format.').required('Username is required.'),
    password: Yup.string().min(6, 'Password at least 6 characters.').required('Password is required.'),
});