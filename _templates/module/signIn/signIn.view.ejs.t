---
to: src/scenes/auth/signIn/SignIn.view.js
---
import { useFormik } from 'formik';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text, TextInput } from 'react-native';
import { SIGN_IN_SCHEME } from './SignIn.constants';
import styles from './SignIn.styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const Input = React.memo(({isNumer, placeholder, isSecure, value, onChangeText, error }) => {
    return (
        <>
            <TextInput keyboardType={isNumer ? "numeric" : "default"} onChangeText={onChangeText} value={value} secureTextEntry={isSecure} placeholder={placeholder} style={styles.textInput} placeholderTextColor="rgba(34, 167, 240, 0.4)" />
            {error && <Text style={styles.txtError}>{error}</Text>}
        </>
    );
});

function SignInView({ onSignIn }) {
    const { handleChange, values, handleSubmit, errors } = useFormik({ initialValues: { username: '', password: '' }, validationSchema: SIGN_IN_SCHEME, onSubmit: onSignIn });
    return (
         <View style={styles.safeView}>
            <Text style={styles.title}>SIGN IN</Text>
            <KeyboardAwareScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <View style={styles.break} />
                    <Input isNumer={<%= type === 'Phone Number' %>} placeholder="<%= type %>" value={values.username} onChangeText={handleChange('username')} error={errors.username} />
                    <View style={styles.break} />
                    <Input placeholder="Password" isSecure value={values.password} onChangeText={handleChange('password')} error={errors.password} />
                    <View style={styles.break} />
                    <TouchableOpacity style={styles.signInBtn} onPress={handleSubmit}>
                        <Text style={styles.btnLabel}>Sign In</Text>
                    </TouchableOpacity>
                </View> 
            </KeyboardAwareScrollView>
        </View>
    );
}

export default React.memo(SignInView);