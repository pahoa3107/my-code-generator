---
to: src/redux/sagas/authSagas/signInSaga.js
---
import {put, call} from 'redux-saga/effects';
import {setAccessToken, loginSuccess} from '@Actions/authActions';
import {signInApi} from '@Apis/authApi';
import {invoke} from '@Helpers/sagas';
import {parseUserInfo} from '@Redux/parsers/authParses';
import APIUtils from '@Utils/apiUtils';

export function* signInSaga({payload, type}) {
  const {showLoading = true, callback = () => {}, params} = payload || {};
  yield invoke(
    function* execution() {
      const result = yield call(signInApi, params);
      const dataParse = parseUserInfo(result);
      const {token} = result;
      APIUtils.setAccessToken(token);
      yield put(setAccessToken(token));
      yield put(signInSuccess(dataParse));
      yield callback(null, dataParse);
    },
    null,
    showLoading,
    type,
  );
}