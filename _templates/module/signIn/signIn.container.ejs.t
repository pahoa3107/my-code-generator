---
to: src/scenes/auth/signIn/SignIn.container.js
---

import React, { useCallback } from 'react';
import SignInView from './SignIn.view';
import { signInSubmit } from '@Redux/actions/authActions';
import { useActions } from '@Hooks/useActions';

export default function SignInContainer() {
    const actions = useActions({ signInSubmit });

    const onSignIn = useCallback((value) => {
        actions.signInSubmit({
                params: value,
            });
    }, [actions]);

    return (
        <SignInView onSignIn={onSignIn} />
    );
};