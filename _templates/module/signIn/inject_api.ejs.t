---
inject: true
to: src/services/apis/authApi.js
skip_if: signInApi
after: "// APIS"
--- 
export const signInApi = () => {
  return utils.get(`${END_POINT}/signIn`);
};