---
inject: true
to: src/redux/sagas/authSagas/index.js
skip_if: AUTH.SIGN_IN.HANDLER
after: "// Sagas"
---
    yield takeLatest(AUTH.SIGN_IN.HANDLER, signInSaga);