---
inject: true
to: src/redux/actions/authActions.js
skip_if: signInSubmit
after: "// Actions"
--- 
export const signInSubmit = (payload) => ({
    type: AUTH.SIGN_IN.HANDLER,
    payload,
});

export const signInSuccess = (payload) => ({
    type: AUTH.SIGN_IN.SUCCESS,
    payload,
});

export const setAccessToken = (token) => ({
    type: AUTH.ACCESS_TOKEN,
    payload: token,
});