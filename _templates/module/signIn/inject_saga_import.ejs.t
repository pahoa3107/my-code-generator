---
inject: true
to: src/redux/sagas/authSagas/index.js
skip_if: ./signInSaga
after: ""
---
import {signInSaga} from './signInSaga';