---
inject: true
to: src/redux/reducers/authReducer.js
skip_if: AUTH.SIGN_IN
after: "// Reducers"
---
        case AUTH.SIGN_IN.SUCCESS: {
            return {
                ...state,
                userInfo: action.payload,
            }
        }