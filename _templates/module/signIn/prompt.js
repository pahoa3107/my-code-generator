module.exports = {
    prompt: ({ prompter }) =>
        prompter
            .prompt([
                {
                    type: 'select',
                    name: 'type',
                    message: 'What type of username?',
                    choices: ['Username', 'Email', 'Phone Number']
                },
            ])
}