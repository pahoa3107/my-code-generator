---
inject: true
to: src/routers/RootNavigator.js
skip_if: name={SCENE_NAMES.SIGN_IN}
after: "Plop screen"
---
        <Stack.Screen
            options={{headerShown: false}}
            name={SCENE_NAMES.SIGN_IN}
            component={SignInContainer}/>
