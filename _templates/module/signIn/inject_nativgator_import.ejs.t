---
inject: true
to: src/routers/RootNavigator.js
skip_if: import SignInContainer
after: "// Screen Import"
---
import SignInContainer from '@Scenes/auth/signIn/signIn.container';
