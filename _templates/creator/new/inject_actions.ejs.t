---
inject: true
to: src/redux/actions/<%= h.changeCase.camel(name) %>Actions.js
skip_if: <%= formattedName %>ItemSubmit
after: // Actions
---
<% formattedName = actionType + h.changeCase.pascal(name) -%>
<% typeName = name.toUpperCase() -%>

export const <%= formattedName %>ItemSubmit = (payload) => ({
    type: <%= typeName %>.<%= actionType.toUpperCase()%>_ITEM.HANDLER,
    payload,
});

export const <%= formattedName %>ItemSuccess = (payload) => ({
    type: <%= typeName %>.<%= actionType.toUpperCase()%>_ITEM.SUCCESS,
    payload,
});