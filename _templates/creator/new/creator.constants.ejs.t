---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= actionType+h.changeCase.pascal(name) %>/<%=h.changeCase.pascal(actionType)+h.changeCase.pascal(name) %>.constants.js
---
import * as Yup from 'yup';

export const INPUT_SCHEME = Yup.object().shape({
    <% fields.map((field) => {if(field.value !== 'id'){%>
    <%= h.changeCase.camelCase(field.value) %>: Yup.string().required('<%= field.value %> is required.'),<%}})%>
});


