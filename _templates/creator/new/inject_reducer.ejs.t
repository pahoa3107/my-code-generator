---
inject: true
to: src/redux/reducers/<%= h.changeCase.camel(name) %>Reducer.js
skip_if: ADD_ITEM.SUCCESS
after: // Reducers
---