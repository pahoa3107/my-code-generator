---
inject: true
to: src/constants/sceneName.js
after: "// Scene name"
skip_if: <%= actionType.toUpperCase() %>_<%=h.inflection.underscore(name).toUpperCase() %>
---
<%= actionType.toUpperCase() %>_<%=h.inflection.underscore(name).toUpperCase() %>: '<%= h.changeCase.pascal(actionType) + h.changeCase.pascal(name) %>Screen',