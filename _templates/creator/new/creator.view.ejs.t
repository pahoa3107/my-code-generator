---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= actionType+h.changeCase.pascal(name) %>/<%= h.changeCase.pascal(actionType)+h.changeCase.pascal(name) %>.view.js
---
<%pascalName= h.changeCase.pascal(actionType) + h.changeCase.pascal(name)-%>
import React, {useMemo} from 'react';
import {SafeAreaView, TextInput, View} from 'react-native';
import styles from './<%=pascalName%>.styles';
import AppText from '@Components/AppText';
import {AppButton, AppIcon} from '@Components';
import {Content} from 'native-base';
import {CREATOR_SCHEME} from './<%=pascalName%>.constants';
import {useFormik} from 'formik';

const Input = React.memo(
  ({placeholder, isSecure, value, onChangeText, error, multiline = false}) => {
    const _inputStyle = useMemo(() => {
      return [styles.textInput, multiline ? styles.multiLineTextInput : null];
    }, [multiline]);
    return (
      <>
        <TextInput
          onChangeText={onChangeText}
          value={value}
          secureTextEntry={isSecure}
          placeholder={placeholder}
          style={_inputStyle}
          multiline={multiline}
        />
        {error && <AppText style={styles.txtError}>{error}</AppText>}
      </>
    );
  },
);
function <%=pascalName%>View({onPressBack, onPressSubmit, initialValues}) {
  const {handleChange, values, handleSubmit, errors, touched} = useFormik({
    <%if(actionType==='update'){%>
    initialValues,
    <%}else{%>
    initialValues: {<%fields.map((field) => {if(field.value !== 'id' && field.value !== 'createAt'){%><%= h.changeCase.camel(field.value) %>: '',<%}})%>},
    <%}%>
    validationSchema: CREATOR_SCHEME,
    onSubmit: onPressSubmit,
  });
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <AppText style={styles.title}><%=pascalName%></AppText>
        <AppIcon type="FontAwesome" name="close" onPress={onPressBack}/>
      </View>
      <Content style={styles.content} bounces={false}>
        <%fields.map((field) => {if(field.value !== 'id' && field.value !== 'createAt'){%><Input
              placeholder={'<%= h.changeCase.pascal(field.value) %>'}
              error={touched.<%=h.changeCase.camel(field.value)%> && errors.<%=h.changeCase.camel(field.value)%>}
              onChangeText={handleChange('<%=h.changeCase.camel(field.value)%>')}
              value={values.<%=h.changeCase.camel(field.value)%>}
              multiline={<%= field.value === 'desc' %>}
            />
          <%}})%>
      </Content>
      <View style={styles.btnView}>
        <AppButton
          title="Submit"
          onPress={handleSubmit}
          style={styles.confirmBtn}
        />
        <AppButton
          title="Cancel"
          onPress={onPressBack}
          style={styles.cancelBtn}
        />
      </View>
    </SafeAreaView>
  );
}

export default React.memo(<%=pascalName%>View);
