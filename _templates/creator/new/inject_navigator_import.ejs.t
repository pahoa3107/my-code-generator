---
inject: true
to: src/routers/RootNavigator.js
skip_if: <%=h.changeCase.pascal(actionType)+h.changeCase.pascal(name)%>Container
after: // Screen Import
---
import <%=h.changeCase.pascal(actionType)+h.changeCase.pascal(name)%>Container from '@Scenes/main/<%= h.changeCase.camel(name) %>/<%=actionType + h.changeCase.pascal(name) %>/<%= h.changeCase.pascal(actionType) + h.changeCase.pascal(name) %>.container';
