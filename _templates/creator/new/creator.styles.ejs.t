---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%=actionType+h.changeCase.pascal(name) %>/<%=h.changeCase.pascal(actionType)+h.changeCase.pascal(name) %>.styles.js
---
import {FONT_FAMILY} from '@Constants/appFonts';
import {DEFAULT_PADDING_HORIZONTAL} from '@Constants/size';
import {scalePortrait} from '@Utils/responsive';
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: DEFAULT_PADDING_HORIZONTAL,
  },
  title: {
    fontSize: scalePortrait(24),
    fontFamily: FONT_FAMILY.BOLD,
  },
  content: {
    padding: DEFAULT_PADDING_HORIZONTAL,
  },
  textInput: {
    borderColor: '#0EAC51',
    borderWidth: scalePortrait(2),
    minHeight: scalePortrait(50),
    marginTop: scalePortrait(15),
    paddingHorizontal: scalePortrait(5),
    borderRadius: scalePortrait(10),
  },
  multiLineTextInput: {
    minHeight: scalePortrait(100),
  },
  txtError: {
    color: 'red',
  },
  btnView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: DEFAULT_PADDING_HORIZONTAL,
    marginBottom: scalePortrait(20)
  },
  confirmBtn: {
    width: scalePortrait(150),
    backgroundColor: '#0EAC51',
  },
  cancelBtn: {
    width: scalePortrait(150),
    backgroundColor: '#808080',
  },
});
