---
inject: true
to: src/routers/RootNavigator.js
skip_if: name={SCENE_NAMES.<%= actionType.toUpperCase() %>_<%=h.inflection.underscore(name).toUpperCase() %>}
after: "Plop screen"
---
        <Stack.Screen
            options={{
                headerShown: false,
                ...TransitionPresets.ModalSlideFromBottomIOS,
            }}
            name={SCENE_NAMES.<%= actionType.toUpperCase() %>_<%=h.inflection.underscore(name).toUpperCase() %>}
            component={<%= h.changeCase.pascal(actionType)+h.changeCase.pascal(name)%>Container}/>
