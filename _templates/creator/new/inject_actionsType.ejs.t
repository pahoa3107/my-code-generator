---
inject: true
to: src/redux/actionsType.js
after: "export const <%= h.inflection.underscore(name).toUpperCase() %> = {"
--- 
    <%=actionType.toUpperCase()%>_ITEM: asyncTypes('<%= h.inflection.underscore(name).toUpperCase() %>/<%=actionType.toUpperCase()%>_ITEM'),