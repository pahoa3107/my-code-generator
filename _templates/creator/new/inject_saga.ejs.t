---
inject: true
to: src/redux/sagas/<%= h.changeCase.camel(name) %>Sagas/index.js
skip_if: "<%= h.inflection.underscore(name).toUpperCase() %>.<%= actionType.toUpperCase()%>_ITEM.HANDLER"
after: export default function*
---
yield takeLatest(<%= h.inflection.underscore(name).toUpperCase() %>.<%= actionType.toUpperCase()%>_ITEM.HANDLER, <%= actionType + h.changeCase.pascal(name) %>ItemSaga);