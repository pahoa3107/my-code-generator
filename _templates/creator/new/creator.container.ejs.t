---
to: src/scenes/main/<%= h.changeCase.camel(name) %>/<%= actionType+h.changeCase.pascal(name) %>/<%= h.changeCase.pascal(actionType)+h.changeCase.pascal(name) %>.container.js
---
<% pascalName = h.changeCase.pascal(actionType)+h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(actionType)+h.changeCase.pascal(name) -%>
import React, {useCallback} from 'react';
import <%= pascalName %>View from './<%= pascalName %>.view';
import NavigationServices from '@Utils/navigationServices';
import {<%= camelName %>ItemSubmit} from '@Redux/actions/<%= h.changeCase.camel(name) %>Actions';
import {useActions} from '@Hooks/useActions';
import {DeviceEventEmitter} from 'react-native';
import {getParams} from '@Utils/navigationServices';

export default function <%= pascalName %>Container({route}) {
    const actions = useActions({<%= camelName %>ItemSubmit});
    <% if(actionType === 'update'){%>
    const {<%= h.changeCase.camel(name) %>Detail} = getParams(route)
    <%}%>
    const onPressSubmit = useCallback((values) => {
        actions.<%= camelName %>ItemSubmit({
            params: values,
            callback: (err, res) => {
                if (!err && res) {
                    NavigationServices.goBack();
                    DeviceEventEmitter.emit('FETCH_LIST');
                }
            },
        })
    }, [actions])

    const onPressBack = useCallback(() => {
        NavigationServices.goBack();
    }, [])
    return (
        <<%= pascalName %>View 
        onPressBack={onPressBack} 
        onPressSubmit={onPressSubmit}
        <% if(actionType === 'update'){%>
        initialValues={<%= h.changeCase.camel(name) %>Detail}
        <%}%>
        />
    )
};
