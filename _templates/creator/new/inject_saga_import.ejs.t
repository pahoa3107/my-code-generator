---
inject: true
to: src/redux/sagas/<%= h.changeCase.camel(name) %>Sagas/index.js
skip_if: "import {<%= actionType + h.changeCase.pascal(name) %>ItemSaga} from"
after: // Sagas Import
---
import {<%= actionType + h.changeCase.pascal(name) %>ItemSaga} from './<%= actionType + h.changeCase.pascal(name) %>ItemSaga';