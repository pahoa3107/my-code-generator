---
inject: true
to: src/services/apis/<%= h.changeCase.camel(name) %>Api.js
skip_if: <%= actionType + h.changeCase.pascal(name) %>ItemApi
after: // Apis
---
export const <%= actionType + h.changeCase.pascal(name) %>ItemApi = (params) => {
    //return utils.post(`${END_POINT}/<%= h.changeCase.camel(name) %>`, params);
    <%if(actionType === 'add'){%>
    const newData = [{...params}].concat(global.dataExample.data);
    global.dataExample = {
        ...global.dataExample,
        data: newData,
    };
    <%}%>
    return {
        success: true,
    };
}