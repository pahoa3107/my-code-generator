---
to: src/redux/sagas/<%= h.changeCase.camel(name) %>Sagas/<%=actionType+h.changeCase.pascal(name) %>ItemSaga.js
---
<% formattedName = actionType + h.changeCase.pascal(name) -%>
<% camelName = h.changeCase.camel(name) -%>
import {call} from 'redux-saga/effects';
import {<%= formattedName %>ItemApi} from '@Services/apis/<%= camelName %>Api';
import {invoke} from '@Helpers/sagas';

export function* <%= formattedName %>ItemSaga({payload, type}) {
  const {showLoading = true, callback = () => {}, params} = payload || {};
  yield invoke(
    function* execution() {
      const result = yield call(<%= formattedName %>ItemApi, params);
      yield callback(null, result);
    },
    null,
    showLoading,
    type,
  );
}
