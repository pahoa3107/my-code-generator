const useActions = {
    path: 'hooks/useActions.js',
    content: () => `import { bindActionCreators } from 'redux'
import { useDispatch } from 'react-redux'
import { useMemo } from 'react'

export function useActions(actions, deps) {
    const dispatch = useDispatch()
    return useMemo(
    () => {
        if (Array.isArray(actions)) {
        return actions.map(a => bindActionCreators(a, dispatch))
        }
        return bindActionCreators(actions, dispatch)
    },
    deps ? [dispatch, ...deps] : [dispatch]
    )
}`
}

const useShallowEqualSelector = {
    path: 'hooks/useShallowEqualSelector.js',
    content: () => `import { useSelector, shallowEqual } from 'react-redux'

export function useShallowEqualSelector(selector) {
    return useSelector(selector, shallowEqual)
}`
}

module.exports = {
    useActions,
    useShallowEqualSelector
}