const reducerTemplate = {
    path: '/reducers/exampleReducer.js',
    content: () => `//import { EXAMPLE } from '../actionTypes';

/*const initialState = {
    exampleState: null,
};*/

/*const exampleReducer = (state = initialState, action) => {
    switch (action.type) {
        case EXAMPLE.ACTION.SUCCESS: {
            return {
                ...state,
                exampleState: action.payload
            };
        }
        default:
            return state;
    }
};*/

//export default exampleReducer;`
}

const indexTemplate = {
    path: '/reducers/index.js',
    content: () => `import AsyncStorage from '@react-native-community/async-storage';
import { combineReducers } from 'redux';
import persistReducer from 'redux-persist/es/persistReducer';
//Import Reducers

//import exampleReducer from './exampleReducer';

//Import Persist Reducer
/*const examplePersistConfig = {
    key: 'example',
    storage: AsyncStorage,
    whitelist: [],
    version: 1.0,
};*/

const rootReducer = combineReducers({
    //example: persistReducer(examplePersistConfig, exampleReducer),
});

export default rootReducer;`
}

module.exports = {
    indexTemplate,
    reducerTemplate
}