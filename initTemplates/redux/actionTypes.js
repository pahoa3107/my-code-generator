
module.exports = {
    path: '/actionTypes.js',
    content: () => `export const asyncTypes = (action) => ({
ORIGIN: action,
SUBMIT: \`$\{action}_SUBMIT\`,
SUCCESS: \`$\{action}_SUCCESS\`,
});

//Import Actions Type

export const EXAMPLE = {
    ACTION: asyncTypes('EXAMPLE/ACTION'),
};`
}