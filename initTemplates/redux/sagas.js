const indexTemplate = {
    path: '/sagas/index.js',
    content: () => `import {fork, all} from "redux-saga/effects";
//Import Sagas
export default function* rootSaga(){
    yield all([
        //Fork Sagas
    ]);
}`
}

const sagaTemplate = {
    path: '/sagas/exampleSagas/exampleSaga.js',
    content: () => `//import {put, call} from 'redux-saga/effects';

/*export function* exampleSaga({payload, type}) {
    //Do something
}*/`
}

const sagaIndexTemplate = {
    path: '/sagas/exampleSagas/index.js',
    content: () => `//import {takeLatest} from 'redux-saga/effects';
/*export default function* exampleSagas () {
    yield takeLatest(EXAMPLE.ACTION.HANDLE, exampleSaga)
}*/`
}

module.exports = {
    indexTemplate,
    sagaTemplate,
    sagaIndexTemplate
}