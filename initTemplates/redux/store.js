module.exports = {
    path: '/store/configureStore.js',
    content: () => `import AsyncStorage from '@react-native-community/async-storage';
import { applyMiddleware, createStore } from 'redux';
import persistReducer from 'redux-persist/es/persistReducer';
import autoMergeLevel1 from 'redux-persist/es/stateReconciler/autoMergeLevel1';
import rootReducer from '../reducers';
import createSagaMiddleware from 'redux-saga';
import persistStore from 'redux-persist/es/persistStore';
import rootSaga from '../sagas';


const bindMiddleware = (middleware) => applyMiddleware(...middleware);

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: [],
    version: 1.0,
    stateReconciler: autoMergeLevel1,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

function configureStore() {
    const middleware = [];
    const sagaMiddleware = createSagaMiddleware();
    middleware.push(sagaMiddleware);
    const store = createStore(persistedReducer, bindMiddleware(middleware));
    const persistor = persistStore(store);
    store.sagaTask = sagaMiddleware.run(rootSaga);

    return { store, persistor };
}

export default configureStore;`
}