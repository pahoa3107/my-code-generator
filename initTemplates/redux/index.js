const actionsTemplate = require("./actions");
const actionsTypesTemplate = require("./actionTypes");
const reducersTemplate = require("./reducers");
const sagasTemplate = require("./sagas");
const selectorsTemplate = require("./selectors");
const storeTemplate = require("./store");
const hooksTemplate = require("./hooks");

module.exports = [{
    basePath: './src/appRedux',
    main: [
        actionsTemplate,
        actionsTypesTemplate,
        reducersTemplate.reducerTemplate,
        reducersTemplate.indexTemplate,
        sagasTemplate.indexTemplate,
        sagasTemplate.sagaIndexTemplate,
        sagasTemplate.sagaTemplate,
        selectorsTemplate,
        storeTemplate,
        hooksTemplate.useActions,
        hooksTemplate.useShallowEqualSelector
    ],
}]
