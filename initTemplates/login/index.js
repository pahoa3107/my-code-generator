const loginView = require("./view");
const loginRedux = require("./redux");
const loginCanInject = require("./canInject");

module.exports = [].concat(
    loginView,
    loginRedux,
    loginCanInject
);