module.exports = {
    basePath: './src/scenes/auth/login',
    main: [{
        path: 'Login.view.js',
        content: () => `/*<-- Login.view.js -->*/
import { useFormik } from 'formik';
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text, TextInput } from 'react-native';
import { LOGIN_SCHEME } from './Login.constant';
import styles from './Login.styles';

const Input = React.memo(({ placeholder, isSecure, value, onChangeText, error }) => {
    return (
        <>
            <TextInput onChangeText={onChangeText} value={value} secureTextEntry={isSecure} placeholder={placeholder} style={styles.textInput} placeholderTextColor="rgba(34, 167, 240, 0.4)" />
            {error && <Text style={styles.txtError}>{error}</Text>}
        </>
    );
});

function LoginView({ onLogin }) {
    const { handleChange, values, handleSubmit, errors } = useFormik({ initialValues: { username: '', password: '' }, validationSchema: LOGIN_SCHEME, onSubmit: onLogin });
    return (
        <View style={styles.container}>
            <Text style={styles.title}>LoginScreen</Text>
            <View style={styles.break} />
            <Input placeholder="Username" value={values.username} onChangeText={handleChange('username')} error={errors.username} />
            <View style={styles.break} />
            <Input placeholder="Password" isSecure value={values.password} onChangeText={handleChange('password')} error={errors.password} />
            <View style={styles.break} />
            <TouchableOpacity style={styles.loginBtn} onPress={handleSubmit}>
                <Text style={styles.btnLabel}>Login</Text>
            </TouchableOpacity>
        </View>
    );
}

export default React.memo(LoginView);`
    },
    {
        path: 'Login.container.js',
        content: () => `/*<-- Login.container.js -->*/
import React, { useCallback } from 'react';
import LoginView from './Login.view';


export default function LoginContainer() {
    const onLogin = useCallback((value) => {
        console.log(value);
    }, []);

    return (
        <LoginView onLogin={onLogin} />
    );
};`
    },
    {
        path: 'Login.styles.js',
        content: () => `/*<-- Login.styles.js -->*/
import { StyleSheet } from 'react-native';
export const MAIN_COLOR = '#22A7F0';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 30,
        backgroundColor: '#fff',
    },
    textInput: {
        width: '100%',
        borderWidth: 1,
        borderColor: MAIN_COLOR,
        color: MAIN_COLOR,
        paddingLeft: 10,
    },
    loginBtn: {
        padding: 10,
        backgroundColor: MAIN_COLOR,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputContainer: {
        width: '100%',
    },
    break: {
        height: 30,
    },
    btnLabel: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 17,
    },
    title: {
        fontSize: 31,
        fontWeight: 'bold',
        color: MAIN_COLOR,
    },
    txtError: {
        color: '#D32F2F',
        alignSelf: 'flex-start',
    },
});`
    },
    {
        path: 'Login.constants.js',
        content: () => `/*<-- Login.constants.js -->*/
import * as Yup from 'yup';

export const LOGIN_SCHEME = Yup.object().shape({
    username: Yup.string()
        // .matches(
        //     /^[0][0-9]{9,9}\b|^[1-9][0-9]{8,8}\b|^[a-z][a-z0-9]{4,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/,
        //     'Invalid email format.')
        .required('Username is required.'),
    password: Yup.string().min(6, 'Password at least 6 characters.').required('Password is required.'),
});`
    },
    ]
}