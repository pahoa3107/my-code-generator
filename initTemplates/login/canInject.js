module.exports = {
    basePath: './src/appRedux',
    main: [{
        path: 'actions/authAction.js',
        content: () => `import {AUTH} from "../actionTypes";

export const loginSubmit = (payload) => ({
    type: AUTH.LOGIN.SUBMIT,
    payload
});

export const loginSuccess = (payload) => ({
    type: AUTH.LOGIN.SUCCESS,
    payload
});`
    },
    {
        path: 'reducers/authReducer.js',
        content: () => `import {AUTH} from "../actionTypes";
const initialState = {
userInfo: {},
accessToken: '',
};

const authReducer = (state = initialState, action) => {
switch (action.type) {
    case AUTH.SIGN_IN.SUCCESS: {
    return {
        ...state,
        userInfo: action.payload,
    };
    }
    case AUTH.SET_ACCESS_TOKEN: {
        return {
            ...state,
            accessToken: action.payload,
        }
    }
    default:
    return state;
}
};

export default authReducer;`
    },
    {
        path: 'sagas/authSagas/index.js',
        content: () => `import {takeLatest} from 'redux-saga/effects';
import {loginSaga} from './loginSaga';
import {AUTH} from '../actionsType';
export default function* authSagas() {
    yield takeLatest(AUTH.LOGIN.SUBMIT, loginSaga);
}`
    },
    {
        path: 'selectors/authSelector.js',
        content: () => `export const getUserInfoSelector = (state) => state.auth.userInfo;
export const getAccessTokenSelector = (state) => state.auth.accessToken;`
    }
    ]
}