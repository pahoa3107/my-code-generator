const login = require('./login');

const redux = require('./redux')

module.exports = {
    login,
    redux
}